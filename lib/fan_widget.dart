import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

import 'package:numberpicker/numberpicker.dart';

class FanWidget extends StatefulWidget {
  DocumentReference ref;
  FanWidget(this.ref);
  @override
  _FanWidgetState createState() => new _FanWidgetState();
}

class _FanWidgetState extends State<FanWidget> {

  Map<String, dynamic> _fanData;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    widget.ref.snapshots.listen((fanDoc) {
      setState(() => _fanData = fanDoc.data);
    });
  }


  @override
  Widget build(BuildContext context) {
    return new ListTile(
      title: new Text(_fanData["name"]),
      trailing: new NumberPicker.integer(
          initialValue: _fanData["speed"],
          minValue: 1,
          maxValue: 5,
          onChanged: (num) {
            widget.ref.updateData({"speed": num});
          },
      ),
    );
  }
}
