import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:lightz/fan_widget.dart';

class DevicesFragment extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // StreamBuilder allows us build a statefulwidget
    // that can by calling builder whenever stream emits a value (snapshot)
    return Column(
      children: <Widget>[
        new Container(
          height: 300.0,
          child: new StreamBuilder(
            // firestore plugin can easily query current snapshot of a collection
            stream: Firestore.instance.collection('home_devices').snapshots,
            builder: buildDevices, // delegate to buildDevices function
          ),
        ),

        new Container(
          height: 100.0,
          child: new StreamBuilder(
            builder: buildFanDevices,
            stream: Firestore.instance.collection('fans').snapshots,
          ),
        )

      ],
    );
  }

  // takes a snapshot, if there's no data, displays a spinner
  // else builds a ListView
  Widget buildDevices(BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
    if(!snapshot.hasData) {
      return new Center(
        child: new CircularProgressIndicator(),
      );
    } else {
      return new ListView.builder(
        itemCount: snapshot.data.documents.length,
        itemBuilder: (BuildContext context, int pos) => buildDevice(context, snapshot.data.documents[pos]),
      );
    }
  }

  // takes snapshot of each device (document in collection)
  // takes the data part and builds SwitchTiles
  Widget buildDevice(BuildContext context, DocumentSnapshot device) {
    Map<String, dynamic> data = device.data;
    return new SwitchListTile(
        value: data["is_on"],
        title: new Text(data["device_name"]),
      onChanged: (val) {
          // update is_on based on switch state
          device.reference.updateData({"is_on": val});
      },
    );
  }
}

Widget buildFanDevices(BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
  if(!snapshot.hasData) {
    return new Center(
      child: new CircularProgressIndicator(),
    );
  } else {
    return new ListView.builder(
      itemCount: snapshot.data.documents.length,
      itemBuilder: (BuildContext context, int pos) => new FanWidget(snapshot.data.documents[pos].reference),
    );
  }
}
